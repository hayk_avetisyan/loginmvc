package com.egs.configuration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
public class Initializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext configApplicationContext=new AnnotationConfigWebApplicationContext();
        configApplicationContext.register(ApplicationConfig.class);
        configApplicationContext.setServletContext(servletContext);
        ServletRegistration.Dynamic servlet=servletContext.addServlet("dispatcher", new DispatcherServlet(configApplicationContext));
                servlet.setLoadOnStartup(1);
                servlet.addMapping("/");
    }
}
