package com.egs;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

@Component
public class ModelView {

    @Bean
    @Qualifier("ModelAndView")
    public org.springframework.web.servlet.ModelAndView getModelAndView(){
        return new org.springframework.web.servlet.ModelAndView();
    }


}
