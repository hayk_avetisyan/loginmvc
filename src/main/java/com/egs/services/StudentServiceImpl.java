package com.egs.services;

import com.egs.dao.StudentDao;
import com.egs.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("studentService")

public class StudentServiceImpl implements StudentService {


    @Autowired
    StudentDao studentDao;

    @Override
    public void addStudent(StudentDTO student) {
        studentDao.addStudent(student);

    }

    @Override
    public void editStudent(StudentDTO person, int studentId) {

    }

    @Override
    public void deleteStudent(int studentId) {

    }

    @Override
    public StudentDTO find(int studentId) {
        return null;
    }

    @Override
    public List<StudentDTO> findAll() {
            return studentDao.findAll();

    }

    @Override
    public StudentDTO findStudentByLogin(String login) {
        return studentDao.findStudentByLogin(login);
    }


}
