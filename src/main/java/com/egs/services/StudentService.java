package com.egs.services;

import com.egs.dto.StudentDTO;

import java.util.List;

public interface StudentService {

    public void addStudent(StudentDTO student);

    public void editStudent(StudentDTO person, int studentId);

    public void deleteStudent(int studentId);

    public StudentDTO find(int studentId);

    public List<StudentDTO> findAll();

    public StudentDTO findStudentByLogin(String login);
}
