package com.egs.dao;


import com.egs.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Repository
@Qualifier("studentDao")
public class StudentDaoImpl implements StudentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Override
    public void addStudent(StudentDTO student) {
        jdbcTemplate.update("INSERT INTO student (studentId, name, surname, login, password) VALUES (?, ?, ?, ?,?)",
                student.getStudentId(), student.getName(), student.getSurname(), student.getLogin(), student.getPassword());
        System.out.println("Person Added!!");
    }

    @Override
    public void editStudent(StudentDTO person, int studentId) {

    }

    @Override
    public void deleteStudent(int studentId) {

    }

    @Override
    public StudentDTO find(int studentId) {
        return null;
    }

    @Override
    public List<StudentDTO> findAll() {
        List <StudentDTO> students = jdbcTemplate.query("SELECT * FROM student", new BeanPropertyRowMapper(StudentDTO.class));
        return students;    }

    @Override
    public StudentDTO findStudentByLogin(String login) {
        StudentDTO studentDTO = (StudentDTO) jdbcTemplate.queryForObject("SELECT * FROM student where login = ? ",
                new Object[] { login }, new BeanPropertyRowMapper(StudentDTO.class));
        return studentDTO;
    }
}
