package com.egs.dao;

import com.egs.dto.StudentDTO;
import java.util.List;

/**
 * Created by haykh on 5/17/2019.
 */
import java.util.List;


public interface StudentDao {

    public void addStudent(StudentDTO student);

    public void editStudent(StudentDTO person, int studentId);

    public void deleteStudent(int studentId);

    public StudentDTO find(int studentId);

    public List<StudentDTO> findAll();

    public StudentDTO findStudentByLogin(String login);

}
