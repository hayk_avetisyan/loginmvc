package com.egs.controllers;

import com.egs.dto.StudentDTO;
import com.egs.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class StudentController {
    @Autowired
    StudentService studentService;


    @RequestMapping(value = "/registered", method = RequestMethod.POST)
    public ModelAndView handleRegistrationRequest(HttpServletRequest request, HttpServletResponse response) {
        String studentId = request.getParameter("studentId");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        StudentDTO studentDTO = new StudentDTO(studentId, name, surname, login, password);
        studentService.addStudent(studentDTO);
        List<StudentDTO> students = studentService.findAll();
        for (StudentDTO student : students) {
            System.out.println(student.getName());
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("registered");
        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView handleLoginRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String login = request.getParameter("login");
        if (studentService.findStudentByLogin(login).getPassword().equals(request.getParameter("password"))) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("login");
            return modelAndView;
        } else {
            return null;
        }

        }

    }



