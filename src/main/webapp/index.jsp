<%--
  Created by IntelliJ IDEA.
  User: aveha
  Date: 5/7/2019
  Time: 12:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<title>Auth</title>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<style>
    body{
        /* background: url("https://www.toptal.com/designers/subtlepatterns/patterns/floor-tile.png"); */
        font-family: 'Source Sans Pro', sans-serif;
    }
    .container{
        width: 100%;
        display:inline-flex;
    }
    .container div{
        width: 50%;
        text-align: center;
    }
    form{
        text-align: center;
        background-color: aliceblue;
        width: 400px;
        border-radius: 10px;
        margin: 0 auto;
        padding: 10px;
        display: grid;
    }
    form input{
        margin: 10px 0;
        height: 25px;
        padding:5px;
        font-size: 15px;
    }
    .submit{
        height: 35px;
        font-size: 20px;
        cursor: pointer;
    }
</style>
</head>
<body>
<h1 style="text-align: center;">Welcome!</h1>
<div class="container">
    <div>
        <form action="/login" method="POST">
            <h3>Login</h3>
            <input type="text" placeholder="Login" name="login">
            <input type="password" placeholder="Password" name="password">
            <input class="submit" type="submit">
        </form>
    </div>
    <div>
        <form action="/registered" method="POST">
            <h3>Register</h3>
            <input type="text" placeholder="StudentId" name="studentId">
            <input type="text" placeholder="Name" name="name">
            <input type="text" placeholder="Surname" name="surname">
            <input type="text" placeholder="login" name="login">
            <input type="password" placeholder="Password" name="password">
            <input class="submit" type="submit">
        </form>
    </div>
</div>
</body>
</html>
